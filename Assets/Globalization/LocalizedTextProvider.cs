using System.Globalization;
using System.Linq;

namespace Globalization
{
    public class LocalizedTextProvider : ILocalizedTextProvider
    {
        private readonly ILocalizedTextReader localizedTextReader;
        private readonly CultureInfo defaultCultureInfo = new CultureInfo("en-US");

        private CultureInfo cultureInfo = new CultureInfo("en-US");

        public LocalizedTextProvider(ILocalizedTextReader localizedTextReader)
        {
            this.localizedTextReader = localizedTextReader;
        }

        public LocalizedTextProvider(ILocalizedTextReader localizedTextReader, CultureInfo defaultCultureInfo)
        {
            this.localizedTextReader = localizedTextReader;
            this.defaultCultureInfo = defaultCultureInfo;
        }

        public void SetCultureInfo(CultureInfo cultureInfo)
        {
            this.cultureInfo = cultureInfo;
        }
        
        public string GetText(string key)
        {
            //Try fetching current locale 
            if (localizedTextReader.Exists(cultureInfo))
            {
                return ReadText(key, cultureInfo);
            }

            //Try fetching parent locale
            //TODO: Check for parent locale

            //Try fetching default locale
            if (localizedTextReader.Exists(defaultCultureInfo))
            {
                return ReadText(key, defaultCultureInfo);
            }

            //We cannot find any translation (X_X)
            return "MISSING KEY: " + key;
        }

        private string ReadText(string key, CultureInfo cultureInfo)
        {
            var textData = localizedTextReader.Read(cultureInfo);
            var textItems = textData.TextItems
                .Where(item => item.Key == key)
                .ToList();
            
            var text = textItems.Any()
                ? textItems.First().Value
                : "MISSING KEY: " + key;

            return text;
        }
    }
}