using System.Globalization;

namespace Globalization
{
    public interface ILocalizedTextReader
    {
        /// <summary>
        /// Checks if a <see cref="TextData"/> json object exists
        /// </summary>
        /// <param name="cultureInfo">The locale which to search for</param>
        /// <returns></returns>
        bool Exists(CultureInfo cultureInfo);

        /// <summary>
        /// Deserializes a <see cref="TextData"/> object from json on disk
        /// </summary>
        /// <param name="cultureInfo">The locale which to return data from</param>
        /// <returns></returns>
        TextData Read(CultureInfo cultureInfo);
    }
}