﻿using UnityEngine;

namespace Globalization
{
	[System.Serializable]
	public class TextItem
	{
		public string Key;
		public string Value;

		public TextItem(string key, string value)
		{
			Key = key;
			Value = value;
		}

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}
	}
}
