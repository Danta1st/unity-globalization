using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

namespace Globalization
{
    public class ResourcesLocalizedTextReader : ILocalizedTextReader
    {
        private readonly string path;

        public ResourcesLocalizedTextReader(string path)
        {
            this.path = path;
        }

        /// <summary>
        /// Checks if a <see cref="TextData"/> json object for the provided locale exists within the directory supplied
        /// </summary>
        /// <param name="cultureInfo">The locale which to search for</param>
        /// <returns></returns>
        public bool Exists(CultureInfo cultureInfo)
        {
            var all = ReadAll(path);
            return all.Any(data => data.CultureInfoName == cultureInfo.Name);
        }

        /// <summary>
        /// Deserializes a <see cref="TextData"/> object from json on disk within the directory supplied
        /// </summary>
        /// <param name="cultureInfo">The locale which to return data from</param>
        /// <returns></returns>
        public TextData Read(CultureInfo cultureInfo)
        {
            var all = ReadAll(path);
            return all.First(data => data.CultureInfoName == cultureInfo.Name);
        }

        private static IEnumerable<TextData> ReadAll(string path)
        {
            var objects = Resources.LoadAll(path, typeof(TextAsset));
            var list = objects.Cast<TextAsset>()
                .Select(data => data.text)
                .Select(json => JsonUtility.FromJson<TextData>(json));
            
            return list;
        }
    }
}