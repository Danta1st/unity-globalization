﻿using System.Globalization;
using Globalization.Editor;
using NUnit.Framework;
using UnityEngine;

namespace Globalization.Tests.Editor
{
	[TestFixture]
	public class LocalizedTextTests 
	{
		private ILocalizedTextReader reader;
		private string path;

		[SetUp]
		public void Setup()
		{
			path = Application.dataPath + "/Resources/LocalizedText";
			reader = new EditorLocalizedTextReader(path);
		}
		
		[Test]
		public void GivenLocaleExists_WhenRead_ThenReturnValid()
		{
			var data = reader.Read(new CultureInfo("da-DK"));
			Assert.IsTrue(data.CultureInfoName == "da-DK");
		}

		[Test]
		public void GivenLocaleDoesNotExist_WhenRead_ThenReturnDefault()
		{
			var provider = new LocalizedTextProvider(reader);
			provider.SetCultureInfo(new CultureInfo("es-ES"));
			
			var text = provider.GetText("CANCEL");
			Assert.AreEqual("Cancel", text);
		}

		[Test]
		public void MyResourcesTest()
		{
			var textReader = new ResourcesLocalizedTextReader("LocalizedText");
			var doesItExists = textReader.Exists(new CultureInfo("en-US"));
			Assert.IsTrue(doesItExists);
		}
	}
}
