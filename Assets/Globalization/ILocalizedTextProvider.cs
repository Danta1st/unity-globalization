namespace Globalization
{
    public interface ILocalizedTextProvider
    {
        string GetText(string key);
    }
}