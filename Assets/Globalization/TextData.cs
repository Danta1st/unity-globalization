using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

namespace Globalization
{
    [System.Serializable]
    public class TextData
    {
        public int Version;
        public string CultureInfoName;
        public TextItem[] TextItems;

        public TextData(int version, CultureInfo cultureInfo, IEnumerable<TextItem> textItems)
        {
            Version = version;
            CultureInfoName = cultureInfo.Name;
            TextItems = textItems.ToArray();
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this, true);
        }
    }
}