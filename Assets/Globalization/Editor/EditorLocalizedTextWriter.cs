using System.IO;
using System.Text;
using UnityEngine;

namespace Globalization.Editor
{
    public class EditorLocalizedTextWriter : ILocalizedTextWriter
    {
        public void Write(string path, TextData textData)
        {
            Directory.CreateDirectory(path);
			
            var json = JsonUtility.ToJson(textData, true);
            var jsonPath = path + "/" + textData.CultureInfoName + ".json";
            using (var writer = new StreamWriter(jsonPath,false, Encoding.UTF8))
            {
                writer.Write(json);
            }
        }
    }
}