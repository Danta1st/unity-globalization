using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;

namespace Globalization.Editor
{
    public class EditorLocalizedTextReader : ILocalizedTextReader
    {
        private readonly string path;

        public EditorLocalizedTextReader(string path)
        {
            this.path = path;
        }
        
        public bool Exists(CultureInfo cultureInfo)
        {
            var jsonPath = GetFilePath(path, cultureInfo);
            return File.Exists(jsonPath);
        }

        public TextData Read(CultureInfo cultureInfo)
        {
            var jsonPath = GetFilePath(path, cultureInfo);
            string json;
            using (var reader = new StreamReader(jsonPath, Encoding.UTF8))
            {
                json = reader.ReadToEnd();
            }

            var textData = JsonUtility.FromJson<TextData>(json);
            return textData;
        }

        private static string GetFilePath(string path, CultureInfo cultureInfo)
        {
            return path + "/" + cultureInfo.Name + ".json";
        }
    }
}