namespace Globalization.Editor
{
    public interface ILocalizedTextWriter
    {
        /// <summary>
        /// Serializes a <see cref="TextData"/> object to json and saves it on disk.
        /// The provided file will be named according to the name of the culture info.
        /// Example: path/en-US.json
        /// </summary>
        /// <param name="path">Absolute path to the directory within which to write</param>
        /// <param name="textData">The data object to write</param>
        void Write(string path, TextData textData);
    }
}