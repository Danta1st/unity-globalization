using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Globalization.Editor
{
    public static class MenuItems
    {
        [MenuItem("Tamsen/Globalization/CreateSamples")]
        public static void CreateLocalizedText()
        {
            Debug.ClearDeveloperConsole();
            Debug.Log("Creating Localized Text Samples...");
            var writer = new EditorLocalizedTextWriter();
            var path = Application.dataPath + "/Resources/LocalizedText";
            
            var dkItems = new List<TextItem>
            {
                new TextItem("ACCEPT", "Accepter"),
                new TextItem("CANCEL", "Fortryd")
            };
            var dkData = new TextData(0, new CultureInfo("da-DK"), dkItems);
            writer.Write(path, dkData);
            
            var usItems = new List<TextItem>
            {
                new TextItem("ACCEPT", "Accept"),
                new TextItem("CANCEL", "Cancel")
            };
            var usData = new TextData(0, new CultureInfo("en-US"), usItems);
            writer.Write(path, usData);
            Debug.Log("Complete. Samples Created at: " + path);
        }
    }
}